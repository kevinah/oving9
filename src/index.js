// @flow
/* eslint eqeqeq: "off" */

import * as React from 'react';
import { Component } from 'react-simplified';
import { HashRouter, Route, NavLink } from 'react-router-dom';
import ReactDOM from 'react-dom';

class Student {
  id: number;
  static nextId = 1;

  firstName: string;
  lastName: string;
  email: string;

  constructor(firstName: string, lastName: string, email: string) {
    this.id = Student.nextId++;
    this.firstName = firstName;
    this.lastName = lastName;
    this.email = email;
  }
}
class Courses{
  id: number;
  static nextId = 1;

  code: string;
  title: string;
  students: Student[];

  constructor(code: string,title:string, students:Student[]){
    this.id = Courses.nextId++;
    this.code=code;
    this.title=title;
    this.students=students;

  }

}
let students = [
  new Student('Ola', 'Jensen', 'ola.jensen@ntnu.no'),
  new Student('Kari', 'Larsen', 'kari.larsen@ntnu.no')
];

let courses=[
  new Courses('TDAT2001', 'Matte 2',[students[0],students[1]]),
  new Courses('TDAT2002', 'Sys.ut 2', [students[1]])
];

console.log(courses);
class Menu extends Component {
  render() {
    return (
      <table>
        <tbody>
          <tr>
            <td>
              <NavLink activeStyle={{ color: 'darkblue' }} exact to="/">
                React example
              </NavLink>
            </td>
            <td>
              <NavLink activeStyle={{ color: 'darkblue' }} to="/students">
                Students
              </NavLink>
            </td>
            <td>
              <NavLink activeStyle={{color:'darkblue'}} to="/courses">
                Courses
              </NavLink>
            </td>
          </tr>
        </tbody>
      </table>
    );
  }
}

class Home extends Component {
  render() {
    return <div>React example with static pages</div>;
  }
}

class CoursesList extends Component{
  render() {
    //console.log(courses);
    //console.log(students);
    return (
      <ul>
        {courses.map( course =>(
          <li key={course.id}>
            <NavLink activeStyle={{color: 'darkblue'}} to={'/courses/'+course.id}>
              {course.title} {course.code}
            </NavLink>
          </li>
        ))}
      </ul>
    )
  }
}

class CourseDetails extends Component<{match: {params: {id:number}}}>{
  render() {
    let course=courses.find(course=>course.id==this.props.match.params.id);
    if(!course){
      console.error('Courses not found');
      return null;
    }
    return (
      <div>
        <h5>{course.code} {course.title}</h5>
        <h6>Students:</h6>
        {course.students.map(student=>(
          <li key={student.id}>
            {student.firstName} {student.lastName}
          </li>
        ))}
      </div>
    );
  }
}

class StudentList extends Component {
  render() {
    return (
      <ul>
        {students.map(student => (
          <li>
            <NavLink activeStyle={{ color: 'darkblue' }} to={'/students/' + student.id}>
              {student.firstName} {student.lastName}
            </NavLink>
          </li>
        ))}
      </ul>
    );
  }
}

class StudentDetails extends Component<{ match: { params: { id: number } } }> {
  render() {
    let student = students.find(student => student.id == this.props.match.params.id);
    if (!student) {
      console.error('Student not found'); // Until we have a warning/error system (next week)
      return null; // Return empty object (nothing to render)
    }
    return (
      <div>
        <ul>
          <li>First name: {student.firstName}</li>
          <li>Last name: {student.lastName}</li>
          <li>Email: {student.email}</li>
        </ul>
      </div>
    );
  }
}

const root = document.getElementById('root');
if (root)
  ReactDOM.render(
    <HashRouter>
      <div>
        <Menu />
        <Route exact path="/" component={Home} />
        <Route path="/students" component={StudentList} />
        <Route path="/students/:id" component={StudentDetails} />
        <Route path="/courses" component={CoursesList}/>
        <Route path="/courses/:id" component={CourseDetails}/>
      </div>
    </HashRouter>,
    root
  );
